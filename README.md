# static linked libraries

This bash script aims to get the files of the static linked libraries

## Before starting

It needs to have installed gdb and run the following commands

```
$ nano ~/.gdbinit
# Add the following and save the file
set debuginfod enabled off
```

The ELF file was generated from the following [repository](https://github.com/daniel5151/ANESE.git), which is a NES emulator I have studying to learn more about emulators.

This project has the following static libraries:

* cfg_path - cross-platform config file
* clara - argument Parsing
* cute_headers - cross-platform directory browsing
* miniz - zipped ROM support
* sdl2 - A/V and Input
* SDL_inprint - SDL fonts, without SDL_ttf
* simpleini - ini config parsing / saving
* stb - image loading / writing

Following the steps it generates a binary file in the folder _build_, which is the one getting analyzed.

## Proposed solution

The bash files can be called as following:

```
$ bash sll.sh path/to/file
```

The output given is:

```
$ bash sll.sh anese              
The longest common prefix is -  /home/emanuelacm/Documentos/redux/ANESE/
Source directory: src
Libraries files:
/home/emanuelacm/Documentos/redux/ANESE/thirdparty/headeronly/clara.hpp
/home/emanuelacm/Documentos/redux/ANESE/thirdparty/headeronly/stb_image_write.h
/home/emanuelacm/Documentos/redux/ANESE/thirdparty/headeronly/stb_image.h
K/home/emanuelacm/Documentos/redux/ANESE/thirdparty/headeronly/cute_files.h
/home/emanuelacm/Documentos/redux/ANESE/thirdparty/headeronly
/home/emanuelacm/Documentos/redux/ANESE/thirdparty/SimpleINI
/home/emanuelacm/Documentos/redux/ANESE/thirdparty/miniz
/home/emanuelacm/Documentos/redux/ANESE/thirdparty/SDL_inprint
/home/emanuelacm/Documentos/redux/ANESE/thirdparty/SDL_inprint/SDL_inprint2.cc
```

## Erklärung des Skripts

Um diese Problem zu lösen, habe ich eine Sckript die die folgende Schritten folgte:

1. Die Ordner und Dateien, die beim Kompiler verwendet wurden, können mit _strings_ abgerufen werden. Das problem ist es dass, _strings_ viele Wörten druck, die nicht Addressen oder Dateipfad sind. Also es ist nötig die Ergebnis zu filtern. Zuerst mit `grep` lässt das Skript nur die Strings, die Dateien von C/C++ sind.
Aber mit dem Ergebnis kann man nicht wissen, ob die Datei aus der Bibliothek ist.

1. Aus diesem Grund, der Skript suche der Pfad des Projekt zu finden. 
Der Pfad ist für alle Dateien derselbe, also ist es möglich, sie zu erhalten, indem man die längste wiederholte Substring ermittelt.
Zum Beispiel:

```
/home/emanuelacm/Documentos/redux/ANESE/src/ui/SDL2/movies/fm2/replay.cc
/home/emanuelacm/Documentos/redux/ANESE/src/ui/SDL2/util/Sound_Queue.cpp
/home/emanuelacm/Documentos/redux/ANESE/thirdparty/headeronly/cute_files.h
/home/emanuelacm/Documentos/redux/ANESE/src/common/serializable.cc

```

Danach ist es wichtig, wieder die Datei mit _strings_ zu analysieren, um ein besseres Ergebnis zu erhalten.

3. Nachfolgend, ist es wichtig die _source_ Ordner zu finden, wo der `main` function ist angeruft. 
Mit `gdb` es ist möglich zu sehen wo der `main` ist angeruft und die Pfad des Datei zu haben:

```
$ gdb -q -ex 'break main' -ex 'quit' $file

Reading symbols from anese...
Breakpoint 1 at 0x18a10: file /home/emanuelacm/Documentos/redux/ANESE/src/ui/SDL2/main.cc, line 3.

```

4. Mit dem Pfad zum Projekt und dem Pfad, in dem die Funktion `main` aufgerufen wird, können die _source_ Ordner gefunden werden.

1. Schließlich kann man mit dem _source_ Pfad nur die Dateien oder Ordner übriglassen, die sich nicht in diesen Ordnern befinden.
Dies lässt die Dateien und Ordner, die Teil der statischen Bibliotheken sind.

## Disclaimer

Leider kann ich nicht sagen, dass diese Lösung mit allen C/C++ Projekten funktionieren würde, weil ich keinen zuverlässigen Weg finden konnte, um festzustellen, ob eine Datei von einem Bibliothek stammt oder nicht.
Aber am mindesten hatte ich ergolft mit diese Projekt.

Ich habe es mit `nm` und `objdump` versucht, aber es war unmöglich, ein Pattern zu finden, mit dem ich feststellen konnte, dass diese Datei von einer Bibliothek stammt.

