#!/bin/sh

file=$1

strings $file | grep -o '/[^"]*.*' | grep -e ".\.c" -e ".\.h" | awk '{ print length, $0 }' | sort -n | awk '{$1=""; print $0}' > strings.txt

commonPrefixUtil() {
    local str1=$1
    local str2=$2
    local len=${#str1}
    if [ ${#str2} -lt $len ]; then
        len=${#str2}
    fi

    result=""
    for ((i = 0; i < len; i++)); do
        if [ "${str1:$i:1}" != "${str2:$i:1}" ]; then
            break
        fi
        result="${result}${str1:$i:1}"
    done

    echo "$result"
}

commonPrefix() {
    local arr=("$@")
    local prefix="${arr[0]}"
    for ((i = 1; i < ${#arr[@]}; i++)); do
        prefix=$(commonPrefixUtil "$prefix" "${arr[i]}")
    done

    echo "$prefix"
}

readarray -t arr < strings.txt

n=${#arr[@]}

ans=$(commonPrefix "${arr[@]}")

if [ ${#ans} -gt 0 ]; then
    echo "The longest common prefix is - $ans"
else
    echo "There is no common prefix"
fi

strings $file | grep ${ans} > strings.txt

main=$(gdb -q -ex 'break main' -ex 'quit' $file | grep -o '/[^"]*.*')

string_length=${#ans}

substring=$(echo "${main}" | cut -c $string_length- )

substring="${substring%%/*}"

echo "Source directory: $substring"

substring_to_remove="${ans}${substring}"
build_folder="${ans}build"

# Remove lines containing the specified substring using grep
echo "Libraries files:"
grep -v $substring_to_remove strings.txt | grep -v $build_folder



