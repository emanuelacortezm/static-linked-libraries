#!/bin/bash

commonPrefixUtil() {
    local str1=$1
    local str2=$2
    local len=${#str1}
    if [ ${#str2} -lt $len ]; then
        len=${#str2}
    fi

    result=""
    for ((i = 0; i < len; i++)); do
        if [ "${str1:$i:1}" != "${str2:$i:1}" ]; then
            break
        fi
        result="${result}${str1:$i:1}"
    done

    echo "$result"
}

commonPrefix() {
    local arr=("$@")
    local prefix="${arr[0]}"
    for ((i = 1; i < ${#arr[@]}; i++)); do
        prefix=$(commonPrefixUtil "$prefix" "${arr[i]}")
    done

    echo "$prefix"
}

readarray -t arr < strings.txt
n=${#arr[@]}

ans=$(commonPrefix "${arr[@]}")

if [ ${#ans} -gt 0 ]; then
    echo "The longest common prefix is - $ans"
else
    echo "There is no common prefix"
fi

