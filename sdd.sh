#!/bin/sh

nm --defined-only -CD -A anese > nm.txt

strings anese | grep -e ".\.c" -e ".\.h" > strings2.txt

strings anese | grep -o '/[^"]*.*' > strings.txt

objdump -p anese  > objdump.txt

gdb -q -ex 'break main' -ex 'quit' anese > gdb.txt
